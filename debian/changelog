xfce4-mount-plugin (1.1.6-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on libxfce4ui-2-dev.

  [ Unit 193 ]
  * New upstream version 1.1.6.
  * Update Standards-Version to 4.6.2.

 -- Unit 193 <unit193@debian.org>  Wed, 09 Aug 2023 20:05:22 -0400

xfce4-mount-plugin (1.1.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.1.5.

 -- Unit 193 <unit193@debian.org>  Sat, 02 Jan 2021 16:58:34 -0500

xfce4-mount-plugin (1.1.4-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Name (from ./configure).
  * Update standards version to 4.4.1, no changes needed.
  * Apply multi-arch hints.
    + xfce4-mount-plugin: Add Multi-Arch: same.

  [ Unit 193 ]
  * d/watch: Update for mirrorbit and use uscan special strings.
  * New upstream version 1.1.4.
    - Fix build with panel 4.15 (Closes: #978213)
  * d/control, d/copyright: Update 'Homepage' field, goodies.xfce → docs.xfce.
  * d/control:
    - Drop libstartup-notification0-dev, libxml-parser-perl
      and libxml2-dev from Build-Depends..
    - R³: no.
  * Update Standards-Version to 4.5.1.

 -- Unit 193 <unit193@debian.org>  Sat, 26 Dec 2020 17:21:27 -0500

xfce4-mount-plugin (1.1.3-2) unstable; urgency=medium

  * d/control: drop build-dep on libxfcegui4
  * d/control: update standards version to 4.3.0
  * d/control: use HTTPS protocol for homepage field
  * d/watch: use HTTPS protocol

 -- Yves-Alexis Perez <corsac@debian.org>  Thu, 27 Dec 2018 16:20:24 +0100

xfce4-mount-plugin (1.1.3-1) unstable; urgency=medium

  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
    closes: #899738
  * Initial upstream branch.
  * d/gbp.conf added, following DEP-14
  * New upstream version 1.1.3

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 24 Aug 2018 14:46:23 +0200

xfce4-mount-plugin (1.1.2-1) unstable; urgency=medium

  [ Mateusz Łukasik ]
  * New upstream release.
  * debian/control:
    - Drop autotools-dev from build depends.
    - update standards version to 4.1.1.
    - Use secured links.
  * debian/compat bumped to 10.

  [ Yves-Alexis Perez ]
  * debian/rules:
    - run dh_autoreconf and xdt-autogen
  * debian/control:
    - add build-dep on xfce4-dev-tools
  * Run wrap-and-sort

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 15 Oct 2017 16:53:28 +0200

xfce4-mount-plugin (0.6.7-1) unstable; urgency=medium

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/control:
    - update standards version to 3.9.5.

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 30 Sep 2014 22:01:37 +0200

xfce4-mount-plugin (0.6.4-2) unstable; urgency=low

  * debian/rules:
    - enable all hardening flags.
  * debian/control:
    - update standards version to 3.9.4.
    - drop version on xfce4-panel-dev build-dep, stable has Xfce 4.8 now.
  * debian/compat bumped to 9.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 25 May 2013 15:00:50 +0200

xfce4-mount-plugin (0.6.4-1) unstable; urgency=low

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 14 May 2012 08:20:37 +0200

xfce4-mount-plugin (0.6.3-1) unstable; urgency=low

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 27 Apr 2012 22:20:11 +0200

xfce4-mount-plugin (0.6.1-1) unstable; urgency=low

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 21 Apr 2012 21:34:02 +0200

xfce4-mount-plugin (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - add build-dep on intltool.
    - replace libxfcegui4 build-dep by libxfce4ui.
    - update debhelper build-dep to 9.
    - add build-dep on dpkg-dev.
    - drop cdbs build-dep.
    - drop build-dep on hardening-includes.
    - update standards version to 3.9.3.
  * debian/rules:
    - replace cdbs by dh and use --parallel.
    - use debhelper 9 and dpkg-dev 1.16.1 hardening support.
    - pass --disable-static to configure.
    - use find to delete .la files
  * debian/compat bumped to 9.

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 17 Apr 2012 21:11:07 +0200

xfce4-mount-plugin (0.5.5-2) unstable; urgency=low

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Yves-Alexis Perez ]
  * debian/watch edited to track Xfce archive reorganisation.
  * debian/control:
    - switch to xfce section.
    - add build-dep on libxfcegui4-dev.
    - update standards version to 3.9.2.
    - update debhelper build-dep to 7.
    - add build-dep on hardening-includes.
    - add dependency on ${misc:Depends}.
  * debian/compat bumped to 7.
  * Switch to 3.0 (quilt) source format.
  * debian/rules:
    - pick {C,LD}FLAGS from dpkg-buildflags.
    - add O1, -z,defs and --as-needed to LDFLAGS.
    - add hardening flags to {C,LD}FLAGS.

  [ Lionel Le Folgoc ]
  * debian/control:
    - add myself to Uploaders.
    - remove Simon and Emanuele from uploaders, thanks to them.
    - bump xfce4-panel-dev b-dep to (>= 4.8.0).

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 19 Apr 2011 23:09:51 +0200

xfce4-mount-plugin (0.5.5-1) unstable; urgency=low

  [ Simon Huggins ]
  * debian/control: Move fake Homepage field to a real one now dpkg
    supports it.
  * Add Vcs-* headers to debian/control

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/control:
    - remove Rudy Godoy from Uploaders. Thanks for the work!
    - update my email address.
    - update standards version to 3.7.3.
  * debian/copyright:
    - update upstream url.
    - update copyright dates.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 17 May 2008 15:05:33 +0200

xfce4-mount-plugin (0.5.4-1) unstable; urgency=low

  (Yves-Alexis Perez)
  * New upstream release.
  (Stefan Ott)
  * updated the url
  (Simon Huggins)
  * Build against latest and greatest xfce.

 -- Yves-Alexis Perez <corsac@corsac.net>  Mon, 01 Oct 2007 22:54:56 +0200

xfce4-mount-plugin (0.4.5-2) unstable; urgency=low

  * Updated build-deps to 4.3.90.2 (Xfce 4.4 Beta2).

 -- Yves-Alexis Perez <corsac@corsac.net>  Wed, 26 Jul 2006 17:48:34 +0100

xfce4-mount-plugin (0.4.5-1) unstable; urgency=low

  * Intial Release.                                            Closes: #370119

 -- Emanuele Rocca <ema@debian.org>  Sat, 03 Jun 2006 14:16:49 +0200
